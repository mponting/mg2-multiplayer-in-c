// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "MG2_TopDownCharacter.generated.h"

UCLASS(Blueprintable)
class AMG2_TopDownCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMG2_TopDownCharacter();

	//called at the beginning of the game
	virtual void BeginPlay() override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	//virtual float TakeDamage(float DamageAmount, struct FDamageEvent const & DamageEvent, class AController * EventInstigator, AActor * DamageCauser) override;

	void MovePlayerToDestination(FVector loc);
	void UpdateMove();

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Location, meta = (AllowPrivateAccess))
		FVector WSTargetLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Location, meta = (AllowPrivateAccess))
		FVector RelativeTargetLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Location, meta = (AllowPrivateAccess))
		FVector RelativeTargetDirection;

	UFUNCTION(Server, Reliable, WithValidation)
		void LaunchBomb();

	UFUNCTION(Server, Reliable, WithValidation)
		void AddScore(APS* TargetPlayerState);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Battle, Replicated)
		float Health;
	
	UPROPERTY(Replicated)
		float MaxHealth;
	
	UPROPERTY(Replicated)
		int BombCount;

	UPROPERTY(Replicated)
		APS* Players_PlayerState;

	UFUNCTION(Server, Reliable, WithValidation)
		void Dead();


protected:
	bool MoveToDest = false;

	UFUNCTION()
		void OnTakeDamage(AActor* Unknown, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, class AActor* DamageCauser);

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UFUNCTION()
	float DistanceToTarget();
};

