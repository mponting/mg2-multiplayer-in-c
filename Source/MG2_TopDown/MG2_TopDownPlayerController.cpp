// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MG2_TopDown.h"
#include "MG2_TopDownPlayerController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MG2_TopDownCharacter.h"
#include "Bomb.h"
#include "PS.h"
#include "MP_Repositories.h"
#include "Blueprint/UserWidget.h"
#include "ScoreWidget_2.h"
#include "MG2_TopDownGameMode.h"
#include "GS.h"
#include "UnrealNetwork.h"
#include "MyGameInstance.h"

AMG2_TopDownPlayerController::AMG2_TopDownPlayerController()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> ScoreWidgetObj(TEXT("/Game/GUI/HUD"));
	if (ScoreWidgetObj.Succeeded()) {
		wHUD = ScoreWidgetObj.Class;
	}

	else {	UE_LOG(LogTemp, Warning, TEXT("ScoreWidget could not be found!")); }

	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
	PlayerName = "Player Name";

}

void AMG2_TopDownPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(AMG2_TopDownPlayerController, DisableMovement);

}

void AMG2_TopDownPlayerController::ClearHUDWidgets_Implementation()
{
	for (TObjectIterator<UUserWidget> Itr; Itr; ++Itr) {
		UUserWidget* LiveWidget = *Itr;

		if (!LiveWidget->GetWorld()) {
			continue;
		}

		else {
			LiveWidget->RemoveFromParent();
		}
	}
}

void AMG2_TopDownPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	if (IsLocalPlayerController()) {
		UE_LOG(LogTemp, Warning, TEXT("Is a local player controller!"));

		if (wHUD && !HUD) {
			HUD = CreateWidget<UScoreWidget_2>(this, wHUD);

			if (HUD) {
				HUD->AddToViewport();
			}
		}
		UE_LOG(LogTemp, Warning, TEXT("Has Authority Check"));
		if (HasAuthority())
		{
			UE_LOG(LogTemp, Warning, TEXT("Has Authority Yes!"));
			AMG2_TopDownGameMode* GM_Ref = Cast<AMG2_TopDownGameMode>(GetWorld()->GetAuthGameMode());
			GM_Ref->UpdatePlayerName(this);
		}

	}


}

void AMG2_TopDownPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}


void AMG2_TopDownPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
	InputComponent->BindAction("DropBomb", IE_Pressed, this, &AMG2_TopDownPlayerController::LaunchBomb);
	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AMG2_TopDownPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AMG2_TopDownPlayerController::OnSetDestinationReleased);
	InputComponent->BindAction("Ready", IE_Released, this, &AMG2_TopDownPlayerController::ToggleReady);
	InputComponent->BindAction("StartGame", IE_Released, this, &AMG2_TopDownPlayerController::BeginGame);
	InputComponent->BindAction("SwitchMap", IE_Released, this, &AMG2_TopDownPlayerController::SwitchMap);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMG2_TopDownPlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AMG2_TopDownPlayerController::MoveToTouchLocation);

	InputComponent->BindAction("ResetVR", IE_Pressed, this, &AMG2_TopDownPlayerController::OnResetVR);
}

void AMG2_TopDownPlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AMG2_TopDownPlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (AMG2_TopDownCharacter* MyPawn = Cast<AMG2_TopDownCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UNavigationSystem::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void AMG2_TopDownPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void AMG2_TopDownPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	if (!DisableMovement) {
		Cast<AMG2_TopDownCharacter>(GetPawn())->MovePlayerToDestination(DestLocation);
	}

}

void AMG2_TopDownPlayerController::LaunchBomb()
{
	if (Cast<AMG2_TopDownCharacter>(GetPawn())) {
		Cast<AMG2_TopDownCharacter>(GetPawn())->LaunchBomb();
	}
	
}

void AMG2_TopDownPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void AMG2_TopDownPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void AMG2_TopDownPlayerController::ToggleReady()
{
	AMG2_TopDownCharacter* PawnReference = Cast<AMG2_TopDownCharacter>(GetPawn());
	APS* PlayerStateReference = Cast<APS>(PlayerState);
	PlayerStateReference->ToggleReady();
}

void AMG2_TopDownPlayerController::BeginGame()
{
	if (HasAuthority()) {
		
		AGameStateBase* GS = GetWorld()->GetGameState();
		if (GS)
		{
			UE_LOG(LogTemp, Warning, TEXT("GameState correct"));
		}
		AGS* OurgameState = Cast<AGS>(GS);
		if (OurgameState) { OurgameState->TriggerCountdowns(); }
		else { UE_LOG(LogTemp, Warning, TEXT("No GameState")); }
	}
}

/** HUD Updating*/
void AMG2_TopDownPlayerController::AddOtherPlayerScores()
{
}

void AMG2_TopDownPlayerController::AddNewScoreUI()
{
}

void AMG2_TopDownPlayerController::RefreshScoreUI(APS* PlayerStateRefreshed)
{
	UE_LOG(LogTemp, Warning, TEXT("Refreshing UI..."));
	if (PlayerStateRefreshed && HUD) {
		HUD->SetPlayerScore(PlayerStateRefreshed->PlayerScore, PlayerStateRefreshed->PlayerNumber);
	}

}

void AMG2_TopDownPlayerController::TriggerCountdown_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("TRIGGERD"));
	if(HUD) { HUD->Countdown(); }
	FTimerHandle FuseTimer;
	GetWorldTimerManager().SetTimer(FuseTimer, this, &AMG2_TopDownPlayerController::TurnOnMovement, 3.0f);
}

void AMG2_TopDownPlayerController::SwitchMap() {
	if (HasAuthority()) {
		AMG2_TopDownGameMode* GM = Cast<AMG2_TopDownGameMode>(GetWorld()->GetAuthGameMode());
		GM->SwitchMaps();
	}

}

bool AMG2_TopDownPlayerController::TriggerCountdown_Validate() {
	return true;
}

void AMG2_TopDownPlayerController::TurnOnMovement()
{
	UE_LOG(LogTemp, Warning, TEXT("Movement has been toggled"));
	if (DisableMovement) {
		DisableMovement = false;
	} 
	
	else { DisableMovement = true; }
}

void AMG2_TopDownPlayerController::UpdatePlayerName() {
	UMyGameInstance* GI_Ref = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());

	APS* PS_Ref = Cast<APS>(PlayerState);
	AGS* GS_Ref = Cast<AGS>(GetWorld()->GetGameState());

	if (PS_Ref) {
		UE_LOG(LogTemp, Warning, TEXT("PS UPDATED!"));
		PS_Ref->UpdatePlayerName(GI_Ref->GetPlayerName());
		//GS_Ref->UpdatePlayerName(this->PlayerName, PS_Ref->PlayerNumber);
	}
}