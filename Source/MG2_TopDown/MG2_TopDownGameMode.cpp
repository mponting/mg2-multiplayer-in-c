// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MG2_TopDown.h"
#include "MG2_TopDownGameMode.h"
#include "MG2_TopDownPlayerController.h"
#include "MG2_TopDownCharacter.h"
#include "MP_Repositories.h"
#include "PS.h"
#include "GS.h"
#include "MyGameInstance.h"

AMG2_TopDownGameMode::AMG2_TopDownGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMG2_TopDownPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	//set the player state class //
	PlayerStateClass = APS::StaticClass();
	GameStateClass = AGS::StaticClass();

}

void AMG2_TopDownGameMode::CheckForGameReady()
{

}

void AMG2_TopDownGameMode::SwitchMaps()
{	
	if (HasAuthority()) {
		
		bUseSeamlessTravel = true;
		GetWorld()->ServerTravel("TopDownExampleMap2?listen");
	}
}

APlayerController* AMG2_TopDownGameMode::ProcessClientTravel(FString& FURL, FGuid NextMapGuid, bool bSeamless, bool bAbsolute) {
	APlayerController* LocalPlayerController = NULL;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = *Iterator;
		if (Cast<UNetConnection>(PlayerController->Player) != NULL)
		{
			/* Check if we're using our Custom Controller */
			AMG2_TopDownPlayerController* MyController = Cast<AMG2_TopDownPlayerController>(PlayerController);
			if (MyController )
			{
				MyController ->ClearHUDWidgets();
			}
 
			// REMOTE PLAYER
			PlayerController->ClientTravel(FURL, TRAVEL_Relative, bSeamless, NextMapGuid);
		}
		else
		{
			// LOCAL PLAYER
			/* Check if we're using a GES Controller */
			AMG2_TopDownPlayerController* MyController = Cast<AMG2_TopDownPlayerController>(PlayerController);
			if (MyController )
			{
				MyController ->ClearHUDWidgets();
			}
 
			LocalPlayerController = PlayerController;
			PlayerController->PreClientTravel(FURL, bAbsolute ? TRAVEL_Absolute : TRAVEL_Relative, bSeamless);
		}
	}
	
	return LocalPlayerController;

}

void AMG2_TopDownGameMode::PostLogin(APlayerController * NewPlayer)
{
	Super::PostLogin(NewPlayer);

	if(!HasAuthority())
	{
		UpdatePlayerName(NewPlayer);
	}

	TArray<AActor*> AllPlayersActive;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMG2_TopDownCharacter::StaticClass(), AllPlayersActive);

	if (AllPlayersActive.Num() >= 4)
	{
		AMG2_TopDownCharacter* PlayerPawn = Cast<AMG2_TopDownCharacter>(NewPlayer->GetPawn());
		PlayerPawn->Dead();
	}
}

void AMG2_TopDownGameMode::UpdatePlayerName(APlayerController * NewPlayer)
{
	if (NewPlayer->HasAuthority())
	{
		printS_1(FString("GM: ") + NewPlayer->GetName() + "is my name");
		AMG2_TopDownPlayerController* MyController = Cast<AMG2_TopDownPlayerController>(NewPlayer);
		APS* PlayerState = Cast<APS>(MyController->PlayerState);
		UMyGameInstance* GInst = Cast<UMyGameInstance>(GetGameInstance());

		print("GM: Game instance fetched maybe we'll check now");
		if (GInst)
		{
			print("GM: game instance found!");
			printS(FString("GM: ") + GInst->PlayerName + " is the Name!");
			PlayerState->UpdatePlayerName(GInst->PlayerName);
			PlayerState->PlayerName = GInst->PlayerName;
			print("GM: Name Update Complete!");
			printS(FString("GM: Player State Name is: ") + PlayerState->GetName());
		}
		else { print("GM: Game Instance is not found!"); }
	}

}