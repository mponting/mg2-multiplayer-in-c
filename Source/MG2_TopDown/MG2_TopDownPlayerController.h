// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/PlayerController.h"
#include "MG2_TopDownPlayerController.generated.h"

UCLASS()
class AMG2_TopDownPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMG2_TopDownPlayerController();

	/**Launch a Bomb **/
	void LaunchBomb();

	/** Makes sure that the player controller is ready to play.*/
	UFUNCTION(BlueprintCallable)
		void ToggleReady();

	UFUNCTION(BlueprintCallable)
		void BeginGame();

	UFUNCTION()
		void AddOtherPlayerScores();

	UFUNCTION()
		void AddNewScoreUI();

	UFUNCTION()
		void RefreshScoreUI(APS* PlayerStateRefreshed);

	UPROPERTY(EditAnywhere, Replicated)
		bool DisableMovement = true;

	UFUNCTION(Server, Reliable, WithValidation)
		void TriggerCountdown();

	UFUNCTION()
		void TurnOnMovement();

	UFUNCTION()
		void SwitchMap();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UScoreWidget_2> wHUD;

	UScoreWidget_2* HUD;

	UFUNCTION(Client, Reliable, Category = "Widget Clear")
		void ClearHUDWidgets();
	virtual void ClearHUDWidgets_Implementation();

	UPROPERTY(EditAnywhere, Replicated)
		FString PlayerName;

	UFUNCTION()
		void UpdatePlayerName();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void BeginPlay() override;
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();


};


