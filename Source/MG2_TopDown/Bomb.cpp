// Fill out your copyright notice in the Description page of Project Settings.

#include "MG2_TopDown.h"
#include "Bomb.h"
#include "MP_Repositories.h"


// Sets default values
ABomb::ABomb()
{
	FVector someVec{ ForceInit };

	//Setup Static Mesh//
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshType(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));
	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComp"));
	if (MeshType.Object) { StaticMeshComp->SetStaticMesh(MeshType.Object); }
	StaticMeshComp->CastShadow = true;
	StaticMeshComp->SetSimulatePhysics(true);
	RootComponent = StaticMeshComp;
		
	//Create Collision//
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(150.f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->SetupAttachment(RootComponent);

	//45.f
	CollisionComp->AddRelativeLocation(FVector(0.f, 0.f, 45.f));

	//Material Setup//
	static ConstructorHelpers::FObjectFinder <UMaterial> BombMaterial(TEXT("Material'/Game/Material/Mat_Base.Mat_Base'"));
	if (BombMaterial.Object) { StaticMeshComp->SetMaterial(0, BombMaterial.Object); }

	//Explosion Emitter//
	ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionParticleAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	ExplosionParticleSystem = ExplosionParticleAsset.Object;

	//Replicate Actor//
	bReplicates = true;
	bReplicateMovement = true;

	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABomb::BeginPlay()
{
	Super::BeginPlay();
	Ignite();
}
	
// Called every frame
void ABomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABomb::Ignite_Implementation()
{
	if (HasAuthority()) {
		FTimerHandle FuseTimer;
		GetWorldTimerManager().SetTimer(FuseTimer, this, &ABomb::Boom, 3.0f);
	}
}

bool ABomb::Ignite_Validate()
{
	return true;
}

void ABomb::Boom_Implementation() {
	FVector ExplosLoc = GetActorLocation();
	FRotator ExplosRot = GetActorRotation();
	UParticleSystemComponent* Explosion = UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionParticleSystem, ExplosLoc, ExplosRot, true);
	Explosion->SetIsReplicated(true);
	Explosion->SetWorldScale3D(FVector(3.f, 3.f, 3.f));

	if(HasAuthority())
	{
		DamagePlayersInRange();
	}
	Destroy();
}

bool ABomb::Boom_Validate()
{
	return true;
}

void ABomb::DamagePlayersInRange_Implementation()
{
	// print("Called Damage Players")
	// Apply damage to anything in a radius that blocks the ECC_Pawn collision channel

	AActor* ContextObject = this;
	float Damage = 25.0f;
	FVector DamageOriginPoint = GetActorLocation();
	float DamageRadius = CollisionComp->GetScaledSphereRadius();
	// Blank list of actors to ignore
	TArray<AActor*> IgnoreActors;
	AActor* DamageCauser = this;
	AController* InstigatorController = GetInstigatorController();
	bool NoDamageFalloff = true;
	ECollisionChannel CollisionChannel = ECC_Pawn;
	UGameplayStatics::ApplyRadialDamage(ContextObject, Damage, DamageOriginPoint, DamageRadius, UDamageType::StaticClass(), IgnoreActors, DamageCauser, InstigatorController, NoDamageFalloff, CollisionChannel);
}
bool ABomb::DamagePlayersInRange_Validate()
{
	return true;
}
