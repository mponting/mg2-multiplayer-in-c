// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "MG2_TopDownGameMode.generated.h"

UCLASS(minimalapi)
class AMG2_TopDownGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMG2_TopDownGameMode();

	UFUNCTION()
		void CheckForGameReady();

	UFUNCTION()
		void SwitchMaps();

	virtual APlayerController* ProcessClientTravel(FString& FURL, FGuid NextMapGuid, bool bSeamless, bool bAbsolute) override;

	void PostLogin(APlayerController *NewPlayer) override;

	void UpdatePlayerName(APlayerController * NewPlayer);

};



