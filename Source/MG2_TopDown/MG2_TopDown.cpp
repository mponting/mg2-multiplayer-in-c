// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MG2_TopDown.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MG2_TopDown, "MG2_TopDown" );

DEFINE_LOG_CATEGORY(LogMG2_TopDown)
 