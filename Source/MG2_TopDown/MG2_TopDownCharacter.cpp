// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "MG2_TopDown.h"
#include "MG2_TopDownCharacter.h"
#include "MP_Repositories.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "Bomb.h"
#include "MP_Repositories.h"
#include "UnrealNetwork.h"
#include "PS.h"

AMG2_TopDownCharacter::AMG2_TopDownCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	Health = 100.f;
	OnTakeAnyDamage.AddDynamic(this, &AMG2_TopDownCharacter::OnTakeDamage);
	
}

void AMG2_TopDownCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(AMG2_TopDownCharacter, Health);
	DOREPLIFETIME(AMG2_TopDownCharacter, MaxHealth);
	DOREPLIFETIME(AMG2_TopDownCharacter, BombCount);
}

void AMG2_TopDownCharacter::OnTakeDamage(AActor * Unknown, float Damage, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("Boom"));
	if (HasAuthority())
	{
		// if current health is greater than the amound of damage we are taking, subtract the damage
		if (Health > Damage)
		{
			Health -= Damage;
		}
		// Otherwise, reset the health
		else
		{
			Health = MaxHealth;
		}

		if (Health <= 0.f) {
			Dead();
		}

		// printS(FString::FromInt(Health));
	}
}

void AMG2_TopDownCharacter::BeginPlay()
{
	Super::BeginPlay();
	this->Players_PlayerState = Cast<APS>(PlayerState);
}

void AMG2_TopDownCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (MoveToDest) {

		if (DistanceToTarget() > 50) {
			UpdateMove();
		}
		else {
			MoveToDest = false;
		}
	}

	else if (MoveToDest) {
		MoveToDest = false;
	}

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			//If the cworld reference is equal to the UWorld.
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params;
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}

		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
}

void AMG2_TopDownCharacter::MovePlayerToDestination(FVector WorldLoc)
{
	// Save this here, for later use
	WSTargetLocation = WorldLoc;

	// Calculate the Target Location in LOCAL SPACE (relative)
	RelativeTargetLocation = WSTargetLocation - GetActorLocation();

	// Store a normalized copy of the Relative Target Location
	RelativeTargetDirection = RelativeTargetLocation;
	RelativeTargetDirection.Z = 0.f;
	RelativeTargetDirection.Normalize();

	MoveToDest = true;
}

void AMG2_TopDownCharacter::UpdateMove()
{
	AddMovementInput(RelativeTargetDirection);
}

float AMG2_TopDownCharacter::DistanceToTarget()
{
	return FVector::DistXY(WSTargetLocation, GetActorLocation());
}

void AMG2_TopDownCharacter::LaunchBomb_Implementation()
{
	FVector BombSpawnLocation = GetActorLocation();
	FRotator BombSpawnRotation = GetActorRotation();

	BombSpawnLocation.X -= FMath::RandRange(-100, 100);
	BombSpawnLocation.Y -= FMath::RandRange(-100, 100);
	BombSpawnLocation.Z -= 10.0;
	GetWorld()->SpawnActor<ABomb>(BombSpawnLocation, BombSpawnRotation);
}

bool AMG2_TopDownCharacter::LaunchBomb_Validate()
{
	return true;
}

void AMG2_TopDownCharacter::AddScore_Implementation(APS* TargetPlayerState) {
	TargetPlayerState->AddOneToScore();
}

bool AMG2_TopDownCharacter::AddScore_Validate(APS* TargetPlayerState)
{
	return true;
}

void AMG2_TopDownCharacter::Dead_Implementation() {

	if (GetWorld() && HasAuthority()) {
		APlayerController* PC_R = Cast <APlayerController>(GetController());
		PC_R->UnPossess();
	
		FVector SPEC_LOC = GetActorLocation();
		FRotator SPEC_ROT = GetActorRotation();

		ASpectatorPawn* SPEC = GetWorld()->SpawnActor<ASpectatorPawn>(SPEC_LOC, SPEC_ROT);

		PC_R->Possess(SPEC);

		this->Destroy();
	}
}

bool AMG2_TopDownCharacter::Dead_Validate() {
	return true;
}