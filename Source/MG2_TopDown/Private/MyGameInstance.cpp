// Fill out your copyright notice in the Description page of Project Settings.

#include "MG2_TopDown.h"
#include "MyGameInstance.h"
#include "MP_Repositories.h"

void UMyGameInstance::SetPlayerName(FString TextName)
{
	
	this->PlayerName = TextName;
}

void UMyGameInstance::AddToTotalCoinCount(int score)
{
	this->GamesWon += score;
}

void UMyGameInstance::AddOneToGamesWon()
{
	this->GamesWon++;
}

FString UMyGameInstance::GetPlayerName() {
	return PlayerName;
}
