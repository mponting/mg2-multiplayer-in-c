// Fill out your copyright notice in the Description page of Project Settings.

#include "MG2_TopDown.h"
#include "GS.h"
#include "MG2_TopDownPlayerController.h"
#include "PS.h"
#include "ScoreWidget_2.h"

void AGS::TriggerCountdowns_Implementation() {
	//Get all player controllers
	//Trigger Countdown

	UE_LOG(LogTemp, Warning, TEXT("Fuck Multiplayer"));

	TArray<AActor*> PlayerControllers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMG2_TopDownPlayerController::StaticClass(), PlayerControllers);

	bool AllReady = true;

	for (AActor* Controller : PlayerControllers) {
		AMG2_TopDownPlayerController* ControllerReference = Cast<AMG2_TopDownPlayerController>(Controller);
		APS* PlayerStateReference = Cast<APS>(ControllerReference->GetPawn()->PlayerState);

		if (!PlayerStateReference || !ControllerReference) {
			continue;
		}

		if (!PlayerStateReference->Ready) {
			UE_LOG(LogTemp, Warning, TEXT("Not all players are ready... stoping..."));
			AllReady = false;
		}
	}

	if (AllReady == true) {
		UE_LOG(LogTemp, Warning, TEXT("All Players are ready to go!"));

		for (AActor* Controller : PlayerControllers) {
			AMG2_TopDownPlayerController* ControllerReference = Cast<AMG2_TopDownPlayerController>(Controller);
			ControllerReference->TriggerCountdown();
		}
	}
}

bool AGS::TriggerCountdowns_Validate() {
	return true;
}

void AGS::UpdatePlayerName_Implementation(const FString & PlayerNmae, int PlayerID)
{
	TArray<AActor*> PlayerControllers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMG2_TopDownPlayerController::StaticClass(), PlayerControllers);

	bool AllReady = true;

	for (AActor* Controller : PlayerControllers) {
		AMG2_TopDownPlayerController* ControllerReference = Cast<AMG2_TopDownPlayerController>(Controller);

		if (ControllerReference) {
			UScoreWidget_2* HUDRefernece = ControllerReference->HUD;
			if (HUDRefernece) { ControllerReference->HUD->SetPlayerName(PlayerNmae, PlayerID); }
		}
	}
}

bool AGS::UpdatePlayerName_Validate(const FString & PlayerNmae, int PlayerID) {
	return true;
}
