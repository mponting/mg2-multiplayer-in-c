// Fill out your copyright notice in the Description page of Project Settings.

#include "MG2_TopDown.h"
#include "MainMenu.h"
#include "engine.h"
#include "UnrealNetwork.h"
#include "MG2_TopDownPlayerController.h"

void UMainMenu::HostGame(int32 ControllerId, FName SessionName, const FString & GameType, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers) 
{
	UGameplayStatics::OpenLevel(this, "TopDownExampleMap", true, "listen");
}

void UMainMenu::JoinGame(FString IP) {
	// APlayerController* PlayerController = Cast<APlayerController>(GetOwningPlayer());
	// FString Command = "open ";
	// Command.Append(IP);
	// PlayerController->ConsoleCommand(Command);

	if (!IP.IsEmpty())
	{
		UGameplayStatics::OpenLevel(this, FName(*IP), true);
		UE_LOG(LogTemp, Warning, TEXT("string is %s"), *IP)
	} else {
		UE_LOG(LogTemp, Warning, TEXT("string is empty, defaulting to localhost"));
		UGameplayStatics::OpenLevel(this, FName("127.0.0.1"), true);
	}

}