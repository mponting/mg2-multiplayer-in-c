// Fill out your copyright notice in the Description page of Project Settings.

#include "MG2_TopDown.h"
#include "PS.h"
#include "UnrealNetwork.h"
#include "MG2_TopDownPlayerController.h"
#include "MP_Repositories.h"
#include "MP_LoggingSystem.h"
#include "MyGameInstance.h"

APS::APS() {
	this->bReplicates = true;
	this->PlayerName = "Test";
	this->NetUpdateFrequency = 70.f;
}

void APS::BeginPlay()
{
	if (HasAuthority()) {
		PlayerNumber = 0;
		TArray<AActor*> AllPlayerStates;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), APS::StaticClass(), AllPlayerStates);

		for (int i = 0; i < AllPlayerStates.Num(); i++) {
			PlayerNumber++;
		}
	}

	UMyGameInstance* GI_Ref = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	this->UpdatePlayerName(GI_Ref->GetPlayerName());
	
}

void APS::ToggleReady_Implementation()
{
	if (this->Ready == true) {
		UE_LOG(LogTemp, Warning, TEXT("Not Ready!"));
		Ready = false;
	}
	else { UE_LOG(LogTemp, Warning, TEXT("Ready!")); Ready = true; }
}

bool APS::ToggleReady_Validate() {
	return true;
}

void APS::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APS, PlayerNumber);
	DOREPLIFETIME(APS, PlayerScore);
	DOREPLIFETIME(APS, Ready);
	DOREPLIFETIME(APS, PlayersName);

}

void APS::AddOneToScore_Implementation() {
	this->PlayerScore += 1;
	UE_LOG(LogTemp, Warning, TEXT("Add One to Score"));
	
	TArray<AActor*> PlayerControllers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMG2_TopDownPlayerController::StaticClass(), PlayerControllers);

	for (AActor* Controller : PlayerControllers) {
		AMG2_TopDownPlayerController* ControllerReference = Cast<AMG2_TopDownPlayerController>(Controller);
		ControllerReference->RefreshScoreUI(this);
	}

	LogDefault("Score 1!");

}

bool APS::AddOneToScore_Validate() {
	return true;
}

void APS::OnRep_PlayerScore()
{

}

void APS::UpdatePlayerName_Implementation(const FString& Text) {
	print("PlayerState: Update Name");
	this->PlayerName = Text;
	printS(FString("PlayerState: ") + this->GetName());
}

bool APS::UpdatePlayerName_Validate(const FString& Text) {
	return true;
}
