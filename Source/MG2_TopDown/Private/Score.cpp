// Fill out your copyright notice in the Description page of Project Settings.

#include "MG2_TopDown.h"
#include "Score.h"
#include "MG2_TopDownCharacter.h"
#include "MP_Repositories.h"
#include "PS.h"
#include "UnrealNetwork.h"

// Sets default values
AScore::AScore()
{
	//Setup Static Mesh//
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshType(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_QuadPyramid.Shape_QuadPyramid'"));
	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComp"));
	if (MeshType.Object) { StaticMeshComp->SetStaticMesh(MeshType.Object); }
	StaticMeshComp->CastShadow = true;
	StaticMeshComp->SetSimulatePhysics(false);
	RootComponent = StaticMeshComp;

	//Create Collision//
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(100.f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->SetupAttachment(RootComponent);
	
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &AScore::OnOverlapBegin);

	//45.f
	CollisionComp->AddRelativeLocation(FVector(0.f, 0.f, 45.f));
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Replicate Actor//
	bReplicates = true;
	bReplicateMovement = true;
	NetUpdateFrequency = 0.f;

}

// Called when the game starts or when spawned
void AScore::BeginPlay() {
	Super::BeginPlay();
	
}

// Called every frame
void AScore::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	FRotator rot(0, 1, 0);
	this->AddActorLocalRotation(rot);
}

void AScore::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void AScore::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (HasAuthority()) {
		if (OtherActor->IsA(AMG2_TopDownCharacter::StaticClass())) {
			AMG2_TopDownCharacter* Character = Cast<AMG2_TopDownCharacter>(OtherActor);
			Character->AddScore(Cast<APS>(Character->PlayerState));
			this->Destroy();
		}
	}
}

void AScore::PreReplication( IRepChangedPropertyTracker & ChangedPropertyTracker ) {
	DOREPLIFETIME_ACTIVE_OVERRIDE(AScore, ReplicatedMovement, bReplicateMovement);
}
