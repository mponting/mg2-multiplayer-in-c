#pragma once

#include <engine.h>

//	GEngine->AddOnScreenDebugMessage([Text Channel],[Time],FColor::[Color],TEXT(x));

#define prefix(p, x) FString(TEXT(p)).Append(FString(TEXT(x)))

#define print(x) if(GEngine){GEngine->AddOnScreenDebugMessage(-1,10.0f,FColor::Yellow, prefix("Debug ", x));}
#define printS(x) if(GEngine){GEngine->AddOnScreenDebugMessage(-1,10.0f,FColor::Yellow, "Debug " + x);}
#define printS_1(x) if(GEngine){GEngine->AddOnScreenDebugMessage(3,10.0f,FColor::Blue, "Debug " + x);}

//#define printM(x) if(GEngine){GEngine->AddOnScreenDebugMessage(2,2.0f,FColor::Red,TEXT("Optimization: " + x));}
//#define printC(x) if(GEngine){GEngine->AddOnScreenDebugMessage(3,2.0f,FColor::Green,TEXT("Client: " + x));}
//#define printS(x) if(GEngine){GEngine->AddOnScreenDebugMessage(4,2.0f,FColor::Blue,TEXT("Server: " + x));}
