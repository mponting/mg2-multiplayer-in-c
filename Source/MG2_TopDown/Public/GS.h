// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "GS.generated.h"

/**
 * 
 */
UCLASS()
class MG2_TOPDOWN_API AGS : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(Server, Reliable, WithValidation)
		void TriggerCountdowns();
	
	UFUNCTION(Server, Reliable, WithValidation)
		void UpdatePlayerName(const FString& PlayerNmae, int PlayerID);
};
