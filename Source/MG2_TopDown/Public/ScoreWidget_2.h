// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "ScoreWidget_2.generated.h"
/**
 * 
 */
UCLASS()
class MG2_TOPDOWN_API UScoreWidget_2 : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
		void SetPlayerScore(int NewPlayerScore, int PlayerID);

	UFUNCTION(BlueprintImplementableEvent)
		void Countdown();

	UFUNCTION(BlueprintImplementableEvent)
		void SetPlayerName(const FString& PlayerName, int PlayerID);

};
