// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MG2_TOPDOWN_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, Category = "Cross Over")
	FString PlayerName;

	UPROPERTY(EditAnywhere, Category = "Cross Over")
	int TotalCoinCount;

	UPROPERTY(EditAnywhere, Category = "Cross Over")
	int GamesWon;
		
	UFUNCTION(BlueprintCallable)
	void SetPlayerName(FString TextName);
	
	UFUNCTION(BlueprintCallable)
	void AddToTotalCoinCount(int score);
	
	UFUNCTION(BlueprintCallable)
	void AddOneToGamesWon();

	UFUNCTION(BlueprintCallable)
		FString GetPlayerName();

};
