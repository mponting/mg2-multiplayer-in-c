// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "PS.generated.h"

UCLASS()
class MG2_TOPDOWN_API APS : public APlayerState
{
	GENERATED_BODY()

public:
	APS();

	UPROPERTY(VisibleAnywhere, Replicated)
	int PlayerNumber;

	UPROPERTY(VisibleAnywhere, ReplicatedUsing = OnRep_PlayerScore)
	int PlayerScore;

	UPROPERTY(VisibleAnywhere, Replicated)
	bool Ready;

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void AddOneToScore();

	UFUNCTION()
	void OnRep_PlayerScore();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable, WithValidation)
	void ToggleReady();

	UFUNCTION(Server, Reliable, WithValidation)
		void UpdatePlayerName(const FString& Text);

	UPROPERTY(VisibleAnywhere, Replicated)
	FString PlayersName;

};
