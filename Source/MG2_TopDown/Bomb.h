// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

UCLASS()
class MG2_TOPDOWN_API ABomb : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	class USphereComponent* CollisionComp;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class UStaticMeshComponent* StaticMeshComp;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class UMaterial* BaseMaterial;

public:	
	// Sets default values for this actor's properties
	ABomb();
	//UMaterialInstanceDynamic* ShaderDynamic;
	UFUNCTION(Server, Reliable, WithValidation)
	void Ignite();

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Boom();

	UFUNCTION(Server, Reliable, WithValidation)
	void DamagePlayersInRange();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UParticleSystem* ExplosionParticleSystem;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
